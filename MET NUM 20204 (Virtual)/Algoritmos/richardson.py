#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------
# Compendio de programas.
# Matemáticas para Ingeniería. Métodos numéricos con Python.
# Copyright (C) 2020 Los autores del texto.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ---------------------------------------------------------------------

# Implementación extrapolación de Richardson y algunos casos de salida.

from math import *


def pol(x):
    """Función de prueba"""
    return x**3 + 4*x**2 - 10  # retorna $pol(x)=x^3+4x^2-10$


def trig(x):
    """Función de prueba"""
    return x*cos(x-1) - sin(x)  # retorna $trig(x)=x\cos(x-1)-\sin(x)$


def dercentrada(f, x, h):
    """Retorna diferencias centradas"""
    return (f(x + h) - f(x - h))/(2*h)


def richardson(f, x, h):
    """
    Implementación extrapolación de Richardson
    Entradas:
    f -- función
    x -- punto
    h -- paso

    Salida:
    d -- aproximación a la derivada
    """
    d = (4/3)*dercentrada(f, x, h/2)-(1/3)*dercentrada(f, x, h)
    return d


# $pol(x)$, $x = 1.5$, $h = 0.1$
print("Derivada función pol(x):")
print("{0:.12f}".format(richardson(pol, 1.5, 0.1)))

# $trig(x)$, $x = 4$, $h = 0.2$
print("Derivada función trig(x):")
print("{0:.12f}".format(richardson(trig, 4, 0.2)))
