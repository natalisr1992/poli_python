#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------
# Compendio de programas.
# Matemáticas para Ingeniería. Métodos numéricos con Python.
# Copyright (C) 2020 Los autores del texto.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ---------------------------------------------------------------------

# Implementación del método de Newton y algunos casos de salida.

from math import *


def expo(x):
    """Función de prueba"""
    return x**2 + exp(-2*x) - 2*x*exp(-x)
    # retorna $expo(x)=x^2+e^{-2x}-2xe^{-x}$


def expoprima(x):
    """Derivada función de prueba"""
    return 2*x - 2*exp(-2*x) - 2*exp(-x) + 2*x*exp(-x)
    # retorna $expoprima(x)=\dfrac{d}{dx}expo(x)$


def trig(x):
    """Función de prueba"""
    return cos(x) - x  # retorna $trig(x)=\cos(x)-x$


def trigprima(x):
    """Derivada función de prueba"""
    return -sin(x) - 1  # retorna $trigprima(x)=\dfrac{d}{dx}trig(x)$


def newton(f, fprima, p0, tol, n):
    """
    Implementación método de Newton
    Entradas:
    f -- función
    fprima -- derivada función f
    p0 -- aproximación inicial
    tol -- tolerancia
    n -- número máximo de iteraciones

    Salida:
    p aproximación a cero de f
    None en caso de iteraciones agotadas
    """
    i = 1
    while i <= n:
        p = p0 - f(p0)/fprima(p0)
        print("Iter = {0:<2}, p = {1:.12f}".format(i, p))
        if abs(p - p0) < tol:
            return p
        p0 = p
        i += 1
    print("Iteraciones agotadas: Error!")
    return None


# $trig(x)$, $trigprima(x)$, $p_0=\frac{\pi}{4}$, $TOL=10^{-8}$, $N_0=100$
print("Newton función trig(x):")
newton(trig, trigprima, pi/4, 1e-8, 100)

# $expo(x)$, $expoprima(x)$, $p_0=4.0$, $TOL=10^{-8}$, $N_0=100$
print("Newton función expo(x):")
newton(expo, expoprima, 4, 1e-8, 100)
