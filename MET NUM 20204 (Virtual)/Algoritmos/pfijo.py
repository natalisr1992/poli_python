#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------
# Compendio de programas.
# Matemáticas para Ingeniería. Métodos numéricos con Python.
# Copyright (C) 2020 Los autores del texto.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ---------------------------------------------------------------------

# Implementación del método del punto fijo y algunos casos de salida.

from math import *


def pote(x):
    """Función de prueba"""
    return pow(2, -x)  # retorna $pote(x)=2^{-x}$


def pol(x):
    """Función de prueba"""
    return (x**2 - 1)/3  # retorna $pol(x)=\frac{x^2-1}{3}$


def puntofijo(f, p0, tol, n):
    """
    Implementación método de punto fijo
    Entradas:
    f -- función
    p0 -- aproximación inicial
    tol -- tolerancia
    n -- número máximo de iteraciones

    Salida:
    p aproximación a punto fijo de f
    None en caso de iteraciones agotadas
    """
    i = 1
    while i <= n:
        p = f(p0)
        print("Iter = {0:<2}, p = {1:.12f}".format(i, p))
        if abs(p - p0) < tol:
            return p
        p0 = p
        i += 1
    print("Iteraciones agotadas: Error!")
    return None


# $pol(x)$, $p_0=0.9$, $TOL=10^{-8}$, $N_0=100$
print("Punto fijo función pol(x):")
puntofijo(pol, 0.9, 1e-8, 100)

# $pote(x)$, $p_0=0.5$, $TOL=10^{-8}$, $N_0=100$
print("Punto fijo función pote(x):")
puntofijo(pote, 0.5, 1e-8, 100)
