# *Jupyter notebooks* para asignaturas de Ciencias Básicas

Para inciar una maquina virtual (reciclable) y ejecutar los jupyter notebooks oprime el siguiente enlace
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/MyLoVoid%2Fpoli_python/master)

**Advertencia:** el lanzamiento inicial del recurso en Binder puede demorar un par de minutos mientras se generan o cargan contenedores o máquinas virtuales en la nube de Binder. Tener paciencia al respecto.

El cuadernillo `Tutorial_Python_v3.ipynb` presenta material introductorio de `Python` y se tienen ejemplos de uso de módulos comunes como es el caso de `numpy`, `matplotlib` y `sympy`. También se cuenta con ejercicios de práctica que pueden servir para una primera entrega.