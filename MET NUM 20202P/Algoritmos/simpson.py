#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------
# Compendio de programas.
# Matemáticas para Ingeniería. Métodos numéricos con Python.
# Copyright (C) 2020 Los autores del texto.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ---------------------------------------------------------------------

# Implementación de la regla de Simpson y algunos casos de salida.

from math import *


def pol(x):
    """Función de prueba"""
    return x**3 + 4*x**2 - 10  # retorna $pol(x)=x^3+4x^2-10$


def trig(x):
    """Función de prueba"""
    return x*cos(x-1) - sin(x)  # retorna $trig(x)=x\cos(x-1)-\sin(x)$


def simpson(f, a, b, n):
    """
    Implementación regla de Simpson
    Entradas:
    f -- función
    a -- inicio intervalo
    b -- fin intervalo
    n -- número de pasos (par)

    Salida:
    abc -- aproximación área bajo la curva
    """
    h = (b - a)/n
    oddsum = 0
    evensum = 0
    for j in range(1, n):
        x = a + h*j
        if j % 2 == 0:
            evensum += 2*f(x)
        else:
            oddsum += 4*f(x)
    abc = (h/3)*(f(a) + evensum + oddsum + f(b))
    return abc


# $pol(x)$, $a = 1$, $b = 2$, $N = 10$
print("Área bajo la curva pol(x):")
print("{0:.12f}".format(simpson(pol, 1, 2, 10)))

# $trig(x)$, $a = 4$, $b = 6$, $N = 20$
print("Área bajo la curva trig(x):")
print("{0:.12f}".format(simpson(trig, 4, 6, 20)))
