#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------
# Compendio de programas.
# Matemáticas para Ingeniería. Métodos numéricos con Python.
# Copyright (C) 2020 Los autores del texto.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ---------------------------------------------------------------------

# Implementación del método de la secante y algunos casos de salida.

from math import *


def trig(x):
    """Función de prueba"""
    return sin(2/x)  # retorna $trig(x)=\sin(2/x)$


def pol(x):
    """Función de prueba"""
    return x**3 - 2  # retorna $pol(x)=x^3-2$


def secante(f, p0, p1, tol, n):
    """
    Implementación método de la secante
    Entradas:
    f -- función
    p0 -- aproximación inicial
    p1 -- aproximación inicial
    tol -- tolerancia
    n -- número máximo de iteraciones

    Salida:
    p aproximación a cero de f
    None en caso de iteraciones agotadas
    """
    i = 2
    while i <= n:
        p = p1 - (f(p1)*(p1 - p0))/(f(p1) - f(p0))
        print("Iter = {0:<2}, p = {1:.12f}".format(i, p))
        if abs(p - p1) < tol:
            return p
        p0 = p1
        p1 = p
        i += 1
    print("Iteraciones agotadas: Error!")
    return None


# $pol(x)$, $p_0=-3.0$, $p_1=3.0$, $TOL=10^{-8}$, $N_0=100$
print("Secante función pol(x):")
secante(pol, -3, 3, 1e-8, 100)

# $trig(x)$, $p_0=1.1$, $p_1=0.8$, $TOL=10^{-8}$, $N_0=100$
print("Secante función trig(x):")
secante(trig, 1.1, 0.8, 1e-8, 100)
